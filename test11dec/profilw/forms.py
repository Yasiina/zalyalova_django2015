__author__ = 'yasina'

from django import forms
from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from common_functions.models import *
from django.forms.models import model_to_dict, fields_for_model
from django.contrib.auth import login as auth_login, logout as auth_logout, authenticate


class LoginForm(forms.Form):
    login = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

class RegistrationForm(forms.ModelForm):

    class Meta:
        model = UserProfile
        fields = '__all__'


class ProfileForm(forms.ModelForm):

    class Meta:
        model = UserProfile
        exclude = ['user']


