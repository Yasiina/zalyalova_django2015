from django.db import models
import datetime
from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.db.models import permalink
import os
# Create your models here.
def get_path(instance, filename):
    return os.path.join(instance.user.username, filename)

class UserProfile(models.Model):
    class Meta:
        db_table = "users_profile"
        verbose_name = _('user_profile')
        verbose_name_plural = "Users"

    user = models.ForeignKey(User, blank=True, null=True)
    photo = models.ImageField(blank=True, upload_to=get_path)


    def __unicode__(self):
        return "%s %s" % (self.user)

class Letters(models.Model):
    class Meta:
        db_table = "letters_table"
        verbose_name_plural = "Letters"

    sender = models.ForeignKey(UserProfile, related_name='send')
    reciver = models.ForeignKey(UserProfile, related_name='recv')
    text = models.CharField(max_length=300)
    date = models.DateField()

    def __unicode__(self):
        return "%s : %s" % (self.sender, self.date)

class Audio(models.Model):
    class Meta:
        db_table = "audios_table"
        verbose_name_plural = "Audio"

    author = models.ForeignKey(UserProfile, related_name='send')
    #music = models.BinaryField()