from django.shortcuts import render

# Create your views here.
from profilw.models import UserProfile, Letters
from django.contrib.auth import login as auth_login, logout as auth_logout, authenticate
from django.db.models import F, Q
from django.http import HttpResponseRedirect as redirect
from django.shortcuts import render
from django.core.urlresolvers import reverse, reverse_lazy
from django.contrib.auth.decorators import login_required
from common_functions.forms import *
from common_functions.models import *
from django.views.generic import TemplateView, UpdateView


def login(request):
    if request.method=="POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            user = authenticate(
                    username=form.cleaned_data["login"],
                    password=form.cleaned_data["password"]
            )
            if user is not None:
                auth_login(request, user)
                if "next" in request.GET:
                    return redirect(request.GET["next"])
                else:
                    return redirect("/statistics/")
            else:
                return render(request, "common_functions/login.html", {"form": form,
                    "errors": ["User is not found."]})
        else:
            return render(request, "common_functions/login.html", {"form": form})
    else:
        form = LoginForm()
        return render(request, "common_functions/login.html", {"form": form})


@login_required(login_url="/login")
def logout(request):
    auth_logout(request)
    return redirect("/login")

def signup(request):
    if request.method=="POST":
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/login")

    else:
        form = RegistrationForm
        return render(request, "profilw/sign_up.html", {"form": form})

class SettingsView(UpdateView):
    form_class = ProfileForm
    model = UserProfile
    template_name = "profilw/settings.html"
    success_url = reverse_lazy("profile")

    def get_object(self, queryset=None):
        return UserProfile.objects.get(user__username=self.request.user)

def main(request):
    return render(request, "profilw/main.html")

def letters(request):
    return render(request, "")

def all_comments(request):
    p = UserProfile.objects.get(user=request.user)
    comments = Letters.objects.filter(reciver=p)
    return render(request, 'profilw/letters.html',{'comments': comments})

