import datetime
from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.db.models import permalink
import os

def get_path(instance, filename):
    return os.path.join(instance.user.username, filename)

class UserProfile(models.Model):
    class Meta:
        db_table = "users_profile"
        verbose_name = _('user_profile')
        verbose_name_plural = "Staff"

    status = (
    ('Teacher', 'Teacher'),
    ('Student', 'Student'))

    user = models.ForeignKey(User, blank=True, null=True)
    user_status = models.CharField(_('status'),max_length=7, choices=status)
    photo = models.ImageField(blank=True, upload_to=get_path)


    def __unicode__(self):
        return "%s %s" % (self.user, self.user_status)


