from django.contrib import admin
from common_functions.models import UserProfile, ClassRoom, Language

admin.site.register(UserProfile)
admin.site.register(Language)
admin.site.register(ClassRoom)

