from django import forms
from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from common_functions.models import *
from django.forms.models import model_to_dict, fields_for_model
from django.contrib.auth import login as auth_login, logout as auth_logout, authenticate


class LoginForm(forms.Form):
    login = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

class RegistrationForm(forms.ModelForm):
    CHOICES = (
        ('Teacher', 'Student'),
    )
    #status = forms.ChoiceField(choices=CHOICES, required=True)
    #classroom = forms.ModelMultipleChoiceField(queryset=ClassRoom.objects.all())
    #password = forms.CharField()
    #image = forms.ImageField()
    '''
     TEST_CHOICES = [[x.language, x.name] for x in ClassRoom.objects.all()]
    status = forms.ChoiceField(choices=CHOICES, required=True)
    classroom = forms.ChoiceField(choices=TEST_CHOICES)
    '''

    class Meta:
        model = UserProfile
        fields = '__all__'
        #fields = ['first_name','last_name', 'username', 'password']


class ProfileForm(forms.ModelForm):

    class Meta:
        model = UserProfile
        #fields = ["first_name", "last_name", "username", "status"]
        exclude = ['user']


