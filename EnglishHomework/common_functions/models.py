import datetime
from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.db.models import permalink
import os

class Language(models.Model):
    class Meta:
        db_table = "languages_table"
        verbose_name = _('lang')
        verbose_name_plural = _('langs')
        ordering = ('language',)

    language = models.CharField(_('language'), max_length=30)

    def __unicode__(self):
        return u'%s' % self.language

class ClassRoom(models.Model):
    language = models.ForeignKey(Language, verbose_name=u'lang')
    name = models.CharField(_('class_name'), max_length=30)

    class Meta:
        verbose_name = _('class_room')
        verbose_name_plural = _('class_rooms')
        db_table = 'class_room'
        #unique_together = ('language', 'name')

    def __unicode__(self):
        return "%s" % (self.name)

def get_path(instance, filename):
    return os.path.join(instance.user.username, filename)

class UserProfile(models.Model):
    class Meta:
        db_table = "users_profile"
        verbose_name = _('user_profile')
        verbose_name_plural = "Staff"

    status = (
    ('Teacher', 'Teacher'),
    ('Student', 'Student'))

    user = models.ForeignKey(User, blank=True, null=True)
    user_status = models.CharField(_('status'),max_length=7, choices=status)
    class_room = models.ManyToManyField(ClassRoom, related_query_name='rooms_set')
    photo = models.ImageField(blank=True, upload_to=get_path)


    def __unicode__(self):
        return "%s" % (self.user)


