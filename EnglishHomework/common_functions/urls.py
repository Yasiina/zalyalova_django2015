from django.conf.urls import url
from common_functions.views import SettingsView

urlpatterns = [
    url(r'^profile$', SettingsView.as_view(), name="settings"),
    url(r'^$', 'common_functions.views.login'),
    url(r'^login$', 'common_functions.views.login'),
    url(r'^signup$', 'common_functions.views.signup', name='signup'),
    url(r'^logout$', 'common_functions.views.logout', name='logout')
]