$(document).ready(function () {

    $("#p").keyup(function (event) {
        document.getElementById("Save").disabled = true;
        if ($(this).val().length < 8) {
            $(this).css("border", "2px solid red")
        } else if (/^[0-9a-zA-Z]{8,}$/.test($(this).val())) {
            $(this).css("border", "2px solid green")

        } else {
            $(this).css("border", "2px solid yellow")

        }
    });
    $("#p2").keyup(function (event) {
        if ($(this).val() == $("#p").val()) {
            $(this).css("border", "2px solid green");
            document.getElementById("Save").disabled = false;
        }else{
            $(this).css("border", "2px solid red")
        }
    });


});