import Image
from django.contrib.auth import login as auth_login, logout as auth_logout, authenticate
from django.db.models import F, Q
from django.http import HttpResponseRedirect as redirect
from django.shortcuts import render
from django.core.urlresolvers import reverse, reverse_lazy
from django.contrib.auth.decorators import login_required
from EnglishHomework import settings
from common_functions.forms import *
from common_functions.models import *
from django.views.generic import TemplateView, UpdateView
from django.http import HttpResponse
from django.utils.datastructures import MultiValueDictKeyError


def login(request):

    if request.method=="POST":
            form = LoginForm(request.POST)
            if form.is_valid():
                user = authenticate(
                        username=form.cleaned_data["username"],
                        password=form.cleaned_data["password"]
                )
                if user is not None:
                    auth_login(request, user)
                    if "next" in request.GET:
                        return redirect(request.GET["next"])
                    else:
                        return redirect("/statistics/")
                else:
                    return render(request, "common_functions/login.html", {"form": form,
                        "errors": ["User is not found."]})
            else:
                return render(request, "common_functions/login.html", {"form": form})

    else:
        form = LoginForm()
        print "noth"
        return render(request, "common_functions/login.html", {"form": form})


@login_required(login_url="/login")
def logout(request):
    auth_logout(request)
    return redirect("/login")

def signup(request):
    if request.method=="POST":
        form = RegistrationForm(request.POST, request.FILES)

        if form.is_valid():
            us = form.cleaned_data["username"]
            p = form.cleaned_data["password"]
            u = User.objects.create_user(us, password=p)
            form = RegistrationForm(request.POST, request.FILES)

            f = form.save(commit=False)

            try:
                t = "%s/%s" % (settings.MEDIA_ROOT, request.FILES['photo'])
                print t
                f.photo = t
            except MultiValueDictKeyError:
                print "f"

            f.user = u
            f.save()
            form.save_m2m()


            user = authenticate(username=u.username, password=p)
            auth_login(request, user)
            return redirect("/statistics/")

    else:
        form = RegistrationForm
        return render(request, "common_functions/sign_up.html", {"form": form})



class SettingsView(UpdateView):
    form_class = ProfileForm
    model = UserProfile
    template_name = "common_functions/settings.html"
    success_url = reverse_lazy("settings")

    def get_object(self, queryset=None):
        return UserProfile.objects.get(user__username=self.request.user)

    def get_context_data(self, **kwargs):
        user = UserProfile.objects.get(user__username=self.request.user)
        context = super(SettingsView, self).get_context_data(**kwargs)
        context["user"] = user
        return context





