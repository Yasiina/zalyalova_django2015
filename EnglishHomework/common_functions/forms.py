from django import forms
from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from common_functions.models import *
from django.forms.models import model_to_dict, fields_for_model
from django.contrib.auth import login as auth_login, logout as auth_logout, authenticate
from django.utils.translation import ugettext_lazy as _

class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

class RegistrationForm(forms.ModelForm):
    username = forms.CharField(max_length=30)
    password = forms.CharField(widget=forms.PasswordInput(attrs={'id':'p'}))
    confirm_password = forms.CharField(widget=forms.PasswordInput(attrs={'id':'p2'}))

    class Meta:
        model = UserProfile
        fields = ['username','password', "confirm_password",'user_status', 'class_room', 'photo']



class ProfileForm(forms.ModelForm):

    class Meta:
        model = UserProfile
        exclude = ['user']





