from django.conf.urls import include, url
from django.contrib import admin
from common_functions.views import SettingsView
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from EnglishHomework import settings

urlpatterns = [

    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('task.urls')),
    url(r'^', include('common_functions.urls'))
]
urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

"""
    url(r'^task(?P<task_id>\d+)?/task_watched_words', 'task.views.', name=''),
    url(r'^(?P<task_id>\d+)?/task_check', 'task.views.', name=''),

    url(r'^(?P<user_id>\d+)?/statistics_student', 'task.views.', name=''),
    url(r'^(?P<task_id>\d+)?/task/task_words?=(?P<task_word_id>\d+)?', 'task.views.', name=''),
    url(r'^(?P<task_id>\d+)?/task/task_test_words?=(?P<task_test_word_id>\d+)?', 'task.views.', name=''),
    url(r'^(?P<task_id>\d+)?/task/task_test(?P<task_test_id>\d+)?', 'task.views.', name=''),
    url(r'^(?P<task_test_result>\d+)?/task_test_result', 'task.views.', name=''),
    url(r'^(?P<task_id>\d+)?/tasks_conditions', 'task.views.', name=''),
    url(r'^(?P<task_id>\d+)?/task_student_answer_edit', 'task.views.', name=''),
    url(r'^(?P<task_id>\d+)?/task', 'task.views.', name=''),
    url(r'^/comments', 'task.views.', name=''),
    url(r'^/comment_to_admin', 'task.views.', name=''),
    url(r'^', 'task.views.', name=''),
"""



