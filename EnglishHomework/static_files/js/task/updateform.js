/**
 * Created by yasina on 14.12.15.
 */
function display_act_ids()
{
    $form=$('#act_ids_form');
    var datastring = $form.serialize();
    $.ajax({
        type: "POST",
        url: $form.attr('action'),
        dataType: 'html',
        data: datastring,
        success: function(result)
        {
            /* The div contains now the updated form */
            $('#act_ids_form_div').html(result);
        }
    });

    //don't submit the form
    return false;
}