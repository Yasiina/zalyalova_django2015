from django.conf.urls import url

urlpatterns = [

    url(r'^statistics/', 'task.views.statistics', name='statistics'),
    url(r'^task/$', 'task.views.task', name='task'),

    url(r'^tasks/(?P<task_id>\d+)?/$', 'task.views.task_view', name='task_id'),
    url(r'^tasks/(?P<task_id>\d+)?/comment/$', 'task.views.comment'),
    url(r'^task_solve/(?P<task_id>\d+)/$', 'task.views.task_solve', name='task_solve'),
    url(r'^add_task/', 'task.views.add_task', name='add_task'),
    url(r'^task_words_test/(?P<task_id>\d+)/$', 'task.views.test_words', name='test_words'),
    url(r'^test_result/(?P<task_id>\d+)/$', 'task.views.words_test_result', name='words_test_result'),


    url(r'^task_check/task_(?P<task_id>\d+)_student_(?P<student_name>.+)/$', 'task.views.check', name='check_task'),
    url(r'^task_words/', 'task.views.words'),
    url(r'^task_test/', 'task.views.test'),

    url(r'^messages$', 'task.views.all_messages', name='messages'),
    url(r'^comments_to_admin$', 'task.views.comments_to_admin', name='comments_to_admin')

]