from django.contrib import admin

# Register your models here.
from task.models import *


admin.site.register(TaskWords)
admin.site.register(TaskWordsTest)
admin.site.register(TaskStudentAnswer)
admin.site.register(TaskTeacherChecked)
admin.site.register(TaskStudentWatchedWords)
admin.site.register(TaskTestResult)
admin.site.register(TaskTest)
admin.site.register(TaskTestVariants)
admin.site.register(Comments)
admin.site.register(MessagesToAdmin)


class TaskAdmin(admin.ModelAdmin):
    list_display = ('title', 'task_type', 'class_room', 'start_date', 'end_date')
    list_filter = ('start_date','task_type',)
admin.site.register(Task, TaskAdmin)