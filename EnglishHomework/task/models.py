from django.db import models
from common_functions.models import ClassRoom, UserProfile
from django.utils.translation import ugettext_lazy as _
import os
import datetime

def get_path(instance, filename):
    return os.path.join('tasks', filename)

class Task(models.Model):
    class Meta:
        db_table = "tasks_table"
        verbose_name = _('task')
        verbose_name_plural = _('tasks')

    type = (
    ('Listening', 'Listening'),
    ('Watching', 'Watching'),
    ('Reading', 'Reading'),
    ('Writing', 'Writing'),
    ('Words', 'Words'),
    ('Test', 'Test')
    )
    class_room = models.ForeignKey(ClassRoom, verbose_name=u'class_room')
    title = models.CharField(max_length=256)
    task_type = models.CharField(max_length=10, choices=type)
    path_to_files = models.FileField(blank=True, upload_to=get_path)
    text = models.CharField(max_length=2000)
    start_date = models.DateField()
    end_date = models.DateField(blank=0, null=True)

    def __unicode__(self):
        return "%s : %s" % (self.class_room, self.title)


class TaskWords(models.Model):
    class Meta:
        db_table = "task_words_table"
        verbose_name = _('task_word')
        verbose_name_plural = _('tasks_words')

    task = models.ForeignKey(Task, verbose_name=u'task')
    first_language = models.CharField(null=False, max_length=40)
    second_language = models.CharField(null=False, max_length=40)
    transcriptions = models.CharField(null=False, max_length=40)

    def __unicode__(self):
        return "%s : %s : %s : %s" % (self.task, self.first_language, self.second_language, self.transcriptions)


class TaskWordsTest(models.Model):
    class Meta:
        db_table = "task_words_test_table"
        verbose_name = _('task_words_test')
        verbose_name_plural = _('tasks_words_tests')

    task_words = models.ForeignKey(TaskWords, verbose_name=u'task_word')
    task = models.ForeignKey(Task, verbose_name=u'task')
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    attempt_amount = models.IntegerField(null=False, default=1)
    time = models.IntegerField(default=10)

    def __unicode__(self):
        return "%s : %s : %s : %s :%s : %s" % (self.task_words, self.task, self.start_date,
                                               self.end_date, self.attempt_amount, self.time)

class TaskStudentAnswer(models.Model):
    class Meta:
        db_table = "task_student_answer_table"
        verbose_name = _('task_student_answer')

    task = models.ForeignKey(Task, verbose_name=u'task')
    student = models.ForeignKey(UserProfile, verbose_name=u'user_profile')
    send_date = models.DateTimeField()
    text = models.CharField(null=False, max_length=300)

    def __unicode__(self):
        return "%s : %s : %s " % (self.task, self.student,
                                               self.text)

class TaskTeacherChecked(models.Model):
    class Meta:
        db_table = "task_teacher_checked_table"
        verbose_name = _('task_teacher_checked')

    task = models.ForeignKey(Task, verbose_name=u'task')
    student = models.ForeignKey(UserProfile, verbose_name=u'user_profile')
    text = models.CharField(max_length=1200)
    mark = models.IntegerField()

    def __unicode__(self):
        return "%s : %s : %s : %s" % (self.task, self.student,
                                               self.text, self.mark)

class TaskStudentWatchedWords(models.Model):
    class Meta:
        db_table = "task_student_watched_words_table"
        verbose_name = _('task_student_watched_words')

    task = models.ForeignKey(Task, verbose_name=u'task')
    student = models.ForeignKey(UserProfile, verbose_name=u'user_profile')
    words_task = models.ForeignKey(TaskWords)

    current_date = models.DateField()
    is_watched = models.BooleanField()

    def __unicode__(self):
        return "%s : %s : %s : %s : %s" % (self.task, self.student,
                                               self.words_task, self.current_date, self.is_watched)

class TaskTestResult(models.Model):
    class Meta:
        db_table = "task_test_result_table"
        verbose_name = _('task_test_result')

    task_test = models.ForeignKey(TaskWordsTest, verbose_name=u'task_words_test')
    student = models.ForeignKey(UserProfile, verbose_name=u'user_profile')

    attempt_count = models.IntegerField(default=1)
    score = models.IntegerField(blank=True)

    def __unicode__(self):
        return "%s : %s : %s : %s" % (self.task_test, self.student,
                                               self.attempt_count, self.score)

class TaskTest(models.Model):
    class Meta:
        db_table = "task_test_table"
        verbose_name = _('task_test')

    task = models.ForeignKey(Task, verbose_name=u'task')
    test_name = models.CharField(max_length=130, null=False, unique=True)

    def __unicode__(self):
        return "%s : %s" % (self.task, self.test_name)


class TaskTestVariants(models.Model):
    class Meta:
        db_table = "task_test_variants_table"
        verbose_name = _('task_test_variants')

    task_test = models.ForeignKey(TaskTest, verbose_name=u'task_test')

    vtype = (
    ('Right', 'Right'),
    ('Wrong', 'Wrong'))

    variant_name = models.CharField(max_length=130, null=False)
    variant_type = models.CharField(max_length=6, choices=vtype)

    def __unicode__(self):
        return "%s : %s : %s" % (self.task_test, self.variant_type, self.variant_name)

class Comments(models.Model):
    class Meta:
        db_table = "comments_table"
        verbose_name_plural = "Comment"

    sender = models.ForeignKey(UserProfile, related_name=u'user_profile')
    reciver = models.ForeignKey(UserProfile, related_name='recv')
    task = models.ForeignKey(Task, related_name=u"task")
    text = models.CharField(max_length=300)
    date = models.DateField()

    def __unicode__(self):
        return "%s : %s : %s" % (self.sender, self.task, self.date)


class MessagesToAdmin(models.Model):
    class Meta:
        db_table = "messages_to_admin_table"
        verbose_name_plural = "Messages To Me"

    sender = models.ForeignKey(UserProfile, related_name=u'sen')
    text = models.CharField(max_length=300)
    date = models.DateField()

    def __unicode__(self):
        return "%s : %s" % (self.sender, self.date)

