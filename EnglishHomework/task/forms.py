from django.contrib.admin.widgets import AdminDateWidget
from django import forms
from django.contrib.admin import widgets
from django.contrib.auth.models import User
from task.models import *


class CommentsForm(forms.Form):
    text = forms.Textarea()

class PostForm(forms.ModelForm):

    class Meta:
        model = Comments
        fields = ['text', 'reciver']
        widgets = {
            'text': forms.TextInput(
                attrs={'id': 'post-text',  'placeholder': 'Say something...'}
            ),
        }

class TextForm(forms.Form):
    text = forms.CharField(widget=forms.Textarea(), label=None)


class TeacherMarkForm(forms.Form):
    text = forms.CharField(widget=forms.Textarea(), label=None)
    mark = forms.IntegerField()

class TaskForm(forms.ModelForm):

    class Meta:
        model = Task
        fields = '__all__'
        widgets = {
            'text': forms.Textarea(
                attrs={'id': 'post-text',  'placeholder': 'Say something...'}
            ),
            'task_type': forms.Select(
                attrs={"onchange":"onProjectChange()"}
            ),
            'start_date': AdminDateWidget(),
            'end_date': AdminDateWidget()
        }

class TestWordsForm(forms.Form):
    def __init__(self, *args, **kwargs):
        questions = kwargs.pop('questions')
        super(TestWordsForm, self).__init__(*args, **kwargs)
        counter = 1
        for q in questions:
            self.fields['question-' + str(counter)] = forms.CharField(label=q.first_language)
            counter += 1

