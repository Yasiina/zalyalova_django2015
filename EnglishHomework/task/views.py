import urllib2
from django.contrib import messages
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
import datetime
from common_functions.models import UserProfile
from task.forms import *
from task.models import *
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
import json
from django.core.urlresolvers import reverse
from django.db.models import F, Q
import os
import re
from EnglishHomework import settings


def add_task(request):
    p = UserProfile.objects.get(user=request.user)
    all_users = User.objects.all()

    post_text = 'empty'
    form = TaskForm
    if request.method=="POST":
     form = TaskForm(request.POST,request.FILES)
     if form.is_valid():
        task = form.save()
        if form.cleaned_data['task_type']=='Words':
            t = "%s/tasks/%s" % (settings.MEDIA_ROOT, request.FILES['path_to_files'])
            print t
            reg_words(t, task)
        if form.cleaned_data['task_type']=='Test':
            our_test = request.FILES['path_to_files']
            t = "%s/tasks/%s" % (settings.MEDIA_ROOT, our_test)
            print t
            reg_test(t, task)

        return HttpResponseRedirect(reverse('statistics'))

    return render(request, 'task/add_task.html',{'form':form, 'post_text': post_text, 'username': request.user.username, 'user':p,
                                                      'all_users':all_users})
def reg_words(path_to, task):
    tx = open(path_to,'r')
    list_for_json = []

    for line in tx:
        matchObj = re.match( r'\d\)(.*)\[(.*)\]\-(.*)', line, re.M|re.I)
        if matchObj:
           print "matchObj.group(1) : ", matchObj.group(1)
           print "matchObj.group(2) : ", matchObj.group(2)
           print "matchObj.group(3) : ", matchObj.group(3)
           t = TaskWords(task=task, first_language=matchObj.group(1), second_language=matchObj.group(3),
                     transcriptions = matchObj.group(2))
           t.save()
           response_data = {}
           response_data['first_language'] = matchObj.group(1)
           response_data['transcription'] = matchObj.group(2)
           response_data['second_language'] = matchObj.group(3)
           list_for_json.append(response_data)

        else:
            print "No match!!"

    return HttpResponse(
                json.dumps(list_for_json),
                content_type="application/json"
            )

def reg_test(path_to, task):

    with open(path_to) as f:
        data = f.read()
        jsondata = json.loads(data)

    for row in jsondata['rows']:
        task_test = TaskTest(task=task, test_name=row['name'])
        task_test.save()
        for key in row['variants']:
            print key
            if row['variants'][key]:
                variant_type='Right'
            else:
                variant_type='Wrong'
            TaskTestVariants(task_test=task_test,
                             variant_name=key, variant_type=variant_type).save()


def task_solve(request, task_id):
    p = UserProfile.objects.get(user=request.user)
    current_task = Task.objects.get(id=task_id)
    comments = Comments.objects.filter(task=current_task)
    all_users = User.objects.all()

    post_text = 'empty'
    form = TextForm
    if request.method == 'POST':
            form = TextForm(request.POST)
            if form.is_valid():
                post_text = form.cleaned_data['text']
                c = TaskStudentAnswer(task = current_task, student = p,
                                      text = post_text,
                                      send_date = datetime.datetime.now())
                c.save()

                request.session["last_tweet"] = 'S'
                return HttpResponseRedirect(reverse('task_id', args=[task_id]))

    return render(request, 'task/task_solution.html',{'form':form, 'post_text': post_text, 'username': request.user.username, 'user':p,
                                                     'current_task': current_task,
                                                     'comments' : comments, 'all_users':all_users})

def statistics(request):
   try:
    p = UserProfile.objects.get(user=request.user)
   except TypeError:
       return redirect("/login")
   tasks = Task.objects.all()
   student_answers = TaskStudentAnswer.objects.all()
   teacher_checked = TaskTeacherChecked.objects.all()

   if p.user_status== 'Teacher':
        return render(request, 'task/statistics_teacher.html',{'username': request.user.username, 'user':p
                                                               ,'tasks': tasks,'student_answers':student_answers,
                                                               'marks':teacher_checked})
   else:
        return render(request, 'task/statistics_student.html',{'username': request.user.username, 'user':p,
                                                                  'tasks': tasks, 'marks':teacher_checked})
def task(request):
    p = UserProfile.objects.get(user=request.user)
    tasks = Task.objects.all()
    return render(request, 'task/task.html',{'username': request.user.username, 'user':p, 'tasks': tasks})

def task_view(request, task_id):

    p = UserProfile.objects.get(user=request.user)
    current_task = Task.objects.get(id=task_id)
    comments = Comments.objects.filter(task=current_task)
    all_users = User.objects.all()

    #path = "%s/%s" % (settings.MEDIA_ROOT, current_task.path_to_files)
    path =  "/media/%s" % (current_task.path_to_files)
    print path


    s = 'empty'
    if "last_tweet" in request.session:
            s = request.session["last_tweet"]
            request.session["last_tweet"] = 'empty'

    form = PostForm
    if request.method=="POST":
        if 'Solve' in request.POST:
            return HttpResponseRedirect(reverse('task_solve', args=[task_id]))

        if 'post_comment' in request.POST:
            form = PostForm(request.POST)
            if form.is_valid():
                r = form.cleaned_data['reciver']

                c = Comments(reciver = r, sender = p, task_id=task_id,
                                 text=form.cleaned_data["text"],
                                 date=datetime.datetime.now())
                c.save()

    q2 = Q(task=current_task)
    if(current_task.task_type=='Words'):
        q = TaskWords.objects.filter(q2)
        if request.method == 'POST':
            try:
                flag = request.POST['flag']
                return HttpResponseRedirect(reverse('test_words', args=[task_id]))
            except KeyError:
                print 'Where is my flag?'
        return render(request, 'task/task_words.html',{ 'form':form, 'username': request.user.username, 'user':p,
                                                       'current_task': q, 'task':current_task,
                                                     'comments' : comments, 'all_users':all_users})
    elif (current_task.task_type=='Test'):
         q = TaskTest.objects.filter(q2)
         q3 = TaskTestVariants.objects.all()


         if 'send' in request.POST:
            print 'im here'
            i = 0
            j = 0
            while i < len(q):
                i+=1
                print 'option' + str(i)
                some_var = request.POST.getlist('option' + str(i) + '[]')
                for s in some_var:
                     print s
                     ttv = TaskTestVariants.objects.get(variant_name=s)
                     if ttv.variant_type == 'Right':
                         print "true"
                         j = j + 1
                     else:
                         print "false"

                TaskStudentAnswer.objects.update_or_create(task=current_task, student=p, send_date=datetime.datetime.now(), text="_")
                TaskTeacherChecked.objects.update_or_create(task=current_task, student=p, text="_", mark = j)

            return render(request,'task/task_test_result.html',
                          {'user':p, 'score': j})


         return render(request, 'task/task_test.html',{'form':form,  'username': request.user.username, 'user':p,
                                                       'current_task': q,
                                                       'q3':q3,
                                                       'comments' : comments, 'all_users':all_users})

    return render(request, 'task/current_task.html',{'path': path,'s':s,'form':form,  'username': request.user.username, 'user':p, 'current_task': current_task,
                                                     'comments' : comments, 'all_users':all_users})

def comment(request, task_id):
    p = UserProfile.objects.get(user=request.user)
    c = Comments(reciver = request.GET['class_chooser_statistics'], sender = p, task_id=task_id,
    	text=request.POST["comment_text"],
    	date=datetime.datetime.now()
    )
    c.save()
    return HttpResponseRedirect("/tasks/" + task_id)


def watched_words(request):
    p = UserProfile.objects.get(user=request.user)
    return render(request, 'task/task_watched_words.html',{'username': request.user.username, 'user':p})


def test_words(request, task_id):
    p = UserProfile.objects.get(user=request.user)
    current_task = Task.objects.get(id = task_id)
    words_task = TaskWords.objects.filter(task = current_task)

    #TaskWordsTest.objects.update_or_create(task_words = words_task, task = current_task)
    if request.method == 'POST':
        try:
            j = 0
            print len(words_task)
            for i in words_task:
                flag = request.POST['answer_text' + str(i.id)]
                print "flag=%s answer_text%d" % (flag, i.id)
                cu_t = TaskWords.objects.get(id = i.id)
                print "cu_t.second_language=%s" % cu_t.second_language
                if flag == cu_t.second_language:
                    j = j + 1

            TaskStudentAnswer.objects.update_or_create(task=current_task, student=p, send_date=datetime.datetime.now(), text="_")
            TaskTeacherChecked.objects.update_or_create(task=current_task, student=p, text="_", mark = j)

            return HttpResponseRedirect(reverse('words_test_result', args=[task_id]))

        except KeyError:
                print 'Where is my flag?'

    return render(request, 'task/task_test_words.html',{'username': request.user.username,
                                                        'user':p, 'words':words_task})


def words_test_result(request, task_id):
    p = UserProfile.objects.get(user=request.user)
    current_task = Task.objects.get(id=task_id)
    ts = TaskTeacherChecked.objects.get(task=current_task, student=p, text="_")
    if request.method == 'POST':
        try:
            back = request.POST['back']
            print 'back'
            return HttpResponseRedirect(reverse('test_words', args=[task_id]))
        except KeyError:
                print 'Where is my flag?'

    return render(request, 'task/task_test_result.html',{'user':p, "score": ts.mark})

def check(request, task_id, student_name):

    p = UserProfile.objects.get(user=request.user)
    user = User.objects.get(username=student_name)
    userpr = UserProfile.objects.get(user=user)
    answer = TaskStudentAnswer.objects.get(student=userpr,task_id=task_id)
    form = TeacherMarkForm(initial={'text': answer.text, 'mark':9})
    current_task = Task.objects.get(id=task_id)

    if request.method=="POST":
        '''if 'Solve' in request.POST:
            return HttpResponseRedirect(reverse('task_solve', args=[task_id]))'''

        if 'Send' in request.POST:
            form = TeacherMarkForm(request.POST)
            print 'i'
            if form.is_valid():
                c = TaskTeacherChecked.objects.update_or_create(
                    task=current_task, student=userpr, text=form.cleaned_data["text"],
                                       mark=form.cleaned_data["mark"])
                return HttpResponseRedirect(reverse('statistics'))



    return render(request, 'task/task_check.html',{'answer_text':answer.text,'form':form,'username': request.user.username,
                                                      'current_task':current_task,'user':p})

def words(request):
    p = UserProfile.objects.get(user=request.user)

    return render(request, 'task/task_words.html',{'username': request.user.username, 'user':p})


def test(request):
    p = UserProfile.objects.get(user=request.user)
    return render(request, 'task/task_test.html',{'username': request.user.username, 'user':p})

def student_answer(request):
    p = UserProfile.objects.get(user=request.user)
    return render(request, 'task/task_student_answer.html',{'username': request.user.username, 'user':p})

def techer_checked(request):
    p = UserProfile.objects.get(user=request.user)
    return render(request, 'task/task_teacher_checked.html',{'username': request.user.username, 'user':p})

def all_messages(request):
    p = UserProfile.objects.get(user=request.user)
    comments = Comments.objects.filter(reciver=p).order_by('-date')

    return render(request, 'task/comments.html',{ 'user':p, 'comments': comments})

def comments_to_admin(request):
    p = UserProfile.objects.get(user=request.user)
    s = "_"
    if request.method == 'POST':
        c = MessagesToAdmin(sender = p,
            text=request.POST["textarea"],
            date=datetime.datetime.now()
        )
        c.save()
        s = 'S'

    return render(request, 'task/comment_to_admin.html',{'username': request.user.username, 'user':p , 's': s})





