__author__ = 'yasina'

import functions, os

working = True

while working:
    instruction = raw_input(":) %s >" % os.path.abspath(os.curdir))
    instr_list = instruction.split()
    current_path = os.curdir

    if instr_list[0] == "exit":
        working = False

    elif instr_list[0] == "ls":
        functions.ls(current_path)

    elif instr_list[0] == "cd":
        if instr_list[1] == "..":
            raw_input(":) %s >" % functions.cd_two_dots(current_path))
        else:
            try:
                raw_input(":) %s >" % functions.cd_func(current_path, instr_list[1]))
            except (OSError, IOError) as e:
                print "Error! This file %s  is not exists" % instr_list[1]



    elif instr_list[0] == "mkdir":
        add_dir = os.path.join(current_path, instr_list[1])
        functions.makedir(add_dir)

    elif instr_list[0] == "copy":
         functions.copy_func(instr_list[1], instr_list[2])

    elif instr_list[0] == "cat":

        try:
         #functions.is_exists(current_path, instr_list[1])
         is_still_text = True
         input_text = []
         while is_still_text:
             text = raw_input(">")
             if(text == "<g"):
                 is_still_text = False
             else:
                input_text.append(text)

         functions.cat_fun(instr_list[1], input_text)

        except(OSError, IOError) as e:
                print "Error! This file %s is not exists" % instr_list[1]

    elif instr_list[0] == "exit":
         functions.exit_fun()


