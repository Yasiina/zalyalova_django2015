__author__ = 'yasina'

import os, shutil


def rm_dir(path):
    os.rmdir(path)

def cd_two_dots(path):
   return os.path.abspath(os.path.join(path, os.pardir))

def cd_func(path, name="lala"):
     answer = os.path.abspath(os.path.join(path, name))
     is_exists(answer, name)
     return answer

def makedir(path):
    os.makedirs(path)

def ls(path):
    for f in os.listdir(path):
            print f

def copy_func(file = "first.py", new_copy_file = "0first.py"):
    shutil.copy(file, new_copy_file)

def cat_fun(filename, some_text):

    with open(filename, 'w') as f:
        for i in some_text:
            f.write(i)

        f.closed

def is_exists(answer, filename):
    if os.path.isfile(answer) == False:
        raise OSError("This %s file is not exists" % filename)

def exit_fun():
    exit(0)


