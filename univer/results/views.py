from django.core import serializers
from django.http import HttpResponse
from django.db.models import F, Q
from django.shortcuts import render
from results.models import StudentExam

# Create your views here.
def show(request):
    # q = StudentExam.objects.raw("select * from results")
    # s = "<br> ".join([str(item) for item in q])
    #q = StudentExam.objects.filter(score__lt=F("planned"))

    q1 = Q(subject="Programming")
    q2 = Q(student="Max")
    q3 = Q(score__gt=F("planed"))
    #student is not Max and scores > planned
    q = StudentExam.objects.filter(~q2 & q3)

    #OR subject is not programming
    #q = StudentExam.objects.filter(~q2 & q3 | ~q1)



    # s = "<br> ".join([str(item) for item in q])

    #serialize as json
    return HttpResponse(serializers.serialize("json", q))

    #losers = StudentExam.objects.exlude(score__gte=F("planned"))
    #loser_of_second_semester = losers.filter(semester = 2)
    #s = "<br> ".join([str(item) for item in loser_of_second_semester])
    #return HttpResponse(s)

    #q = StudentExam.objects.filter(~q2)
   # return HttpResponse(q.values("score", "subjects", "semester"))

def info(request):
    q = StudentExam.objects.all()
    return render(request, 'results/index.html',{'results':q})

def info_planned_(request, mplaned, msemester):
  #  semester=1
    all_planned = Q(score__gt=F("planed"))
    all_semesters = Q(semester=msemester)

   # answer = planned.filter(semester=semester_num)
    q = StudentExam.objects.filter(planed = mplaned, semester =msemester)
    return render(request, 'results/planned_score.html',{'results':q})
