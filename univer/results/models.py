from django.db import models

# Create your models here.
class StudentSubjects(models.Model):
    class Meta:
        abstract = True
    student = models.CharField(max_length=20)
    subject = models.CharField(max_length=20)

class StudentExam(StudentSubjects):
    class Meta:
        db_table = "results"
        verbose_name_plural = "resultados"
        unique_together = ('student', 'subject', 'semester')

    semester = models.SmallIntegerField(default=1)
    planed = models.SmallIntegerField(default=100)
    score = models.SmallIntegerField()

    def __unicode__(self):
        return "%s:%s:%s, planned: %s, got: %s" % (self.student, self.subject, self.semester, self.planed, self.score)